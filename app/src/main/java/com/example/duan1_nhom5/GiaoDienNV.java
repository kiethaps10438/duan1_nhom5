package com.example.duan1_nhom5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;


import com.example.duan1_nhom5.fragment.DienThoaiBan;
import com.example.duan1_nhom5.fragment.HangKho;
import com.example.duan1_nhom5.fragment.HoaDon;
import com.example.duan1_nhom5.fragment.QLNhanVien;
import com.example.duan1_nhom5.fragment.ThongKe;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class GiaoDienNV extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_nv);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomquannv);
        bottomNavigationView.setOnNavigationItemSelectedListener(navi);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navi =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment fragment = null;
                    switch (menuItem.getItemId()){
                        case R.id.hoadon:
                            fragment = new HoaDon();
                            break;
                        case R.id.sp:
                            fragment = new DienThoaiBan();
                            break;
                        case R.id.kho:
                            fragment = new HangKho();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentnv,fragment).commit();
                    return true;
                }
            };
    }

