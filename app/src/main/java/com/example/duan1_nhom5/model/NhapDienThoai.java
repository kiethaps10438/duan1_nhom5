package com.example.duan1_nhom5.model;

public class NhapDienThoai {

    private String MaDT_Nhap;
    private String TenDT_Nhap;
    private String SoLuong_Nhap;
    private String Gia_Nhap;

    public String getMaDT_Nhap() {
        return MaDT_Nhap;
    }

    public void setMaDT_Nhap(String maDT_Nhap) {
        MaDT_Nhap = maDT_Nhap;
    }

    public String getTenDT_Nhap() {
        return TenDT_Nhap;
    }

    public void setTenDT_Nhap(String tenDT_Nhap) {
        TenDT_Nhap = tenDT_Nhap;
    }

    public String getSoLuong_Nhap() {
        return SoLuong_Nhap;
    }

    public void setSoLuong_Nhap(String soLuong_Nhap) {
        SoLuong_Nhap = soLuong_Nhap;
    }

    public String getGia_Nhap() {
        return Gia_Nhap;
    }

    public void setGia_Nhap(String gia_Nhap) {
        Gia_Nhap = gia_Nhap;
    }

    public NhapDienThoai() {
    }

    public NhapDienThoai(String maDT_Nhap, String tenDT_Nhap, String soLuong_Nhap, String gia_Nhap) {
        MaDT_Nhap = maDT_Nhap;
        TenDT_Nhap = tenDT_Nhap;
        SoLuong_Nhap = soLuong_Nhap;
        Gia_Nhap = gia_Nhap;
    }
}
