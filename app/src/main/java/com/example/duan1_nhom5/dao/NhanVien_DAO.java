package com.example.duan1_nhom5.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.duan1_nhom5.database.DBHelper;
import com.example.duan1_nhom5.model.NhanVien;

import java.util.ArrayList;

public class NhanVien_DAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_NHANVIEN="CREATE TABLE NHANVIEN(TenNV text," +
            "MaNV text primary key, NgaySinh date not null, NgayVaoLam date not null, Luong float not null, SDT text not null, MatKhau text not null)";
    public NhanVien_DAO(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    // Thêm NhanVien
    public long addNhanVien(NhanVien nhanVien){
        SQLiteDatabase database;
        db=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TenNV",nhanVien.getTenNV());
        values.put("MaNV", nhanVien.getMaNV());
        values.put("NgaySinh", nhanVien.getNgaySinh());
        values.put("NgayVaoLam", nhanVien.getNgayVaoLam());
        values.put("SDT", nhanVien.getSDT());
        values.put("MatKhau", nhanVien.getMatKhau());
        return db.insert("NhanVien", null, values);
    }
    // Lấy tất cả Nhan Vien
    public ArrayList<NhanVien> getAllnhanvien(){
        ArrayList<NhanVien> danhsachnhanvien = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cr = db.query("NhanVien", null, null,
                null, null, null, null);
        cr.moveToFirst();
        while (!cr.isAfterLast()){
            String TenNV = cr.getString(0);
            String MaNV = cr.getString(1);
            String NgaySinh = cr.getString(2);
            String NgayVaoLam = cr.getString(3);
            String Luong = cr.getString(4);
            String SDT = cr.getString(5);
            String MatKhau = cr.getString(6);

            danhsachnhanvien.add(new NhanVien(TenNV, MaNV, NgaySinh,NgayVaoLam,Luong,SDT,MatKhau));
            cr.moveToNext();
        }return danhsachnhanvien;
    }
    // check trùng MaNV khi thêm vào database
    public Boolean checkMaNV(String MaNV){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from NhanVien where MaNV=?", new String[]{MaNV});
        if(cursor.getCount()>0) return false;
        else return true;
    }
    //check trung sdt
    public Boolean checkSDT(String SDT){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from NhanVien where SDT=?", new String[]{SDT});
        if(cursor.getCount()>0) return false;
        else return true;
    }
    // Cập nhật NhanVien
    public int updateNhanVien(NhanVien nv){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("TenNV",nv.getTenNV());
        values.put("MaNV", nv.getMaNV());
        values.put("NgaySinh", nv.getNgaySinh());
        values.put("NgayVaoLam", nv.getNgayVaoLam());
        values.put("Luong", nv.getLuong());
        values.put("SDT", nv.getSDT());
        values.put("MatKhau", nv.getMatKhau());

        return  db.update("NhanVien", values, "MaNV=?", new
                String[]{nv.getMaNV()});
    }
    // Xóa Nhanvien
    public int deleteNhanVien(NhanVien nv){
        return  db.delete("NhanVien", "MaNV=?", new
                String[]{String.valueOf(nv.getMaNV())});
    }
    //Kiểm tra đăng nhập
    public boolean checkLoginNhanVien(String SDT, String MatKhau){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM NhanVien WHERE SDT=? AND MatKhau=?",
                new String[]{SDT, MatKhau});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
}
