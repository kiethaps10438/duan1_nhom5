package com.example.duan1_nhom5.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.duan1_nhom5.database.DBHelper;
import com.example.duan1_nhom5.model.NhapDienThoai;

public class NhapDienThoai_DAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_NHAPDIENTHOAI="CREATE TABLE NHAPDIENTHOAI(TenDT_Nhap text," +
            "MaDT_Nhap text primary key, SoLuong_Nhap int not null, Gia_Nhap float not null)";
    public NhapDienThoai_DAO(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    // them dien thoai nhap
    public long addDienThoaiNhap(NhapDienThoai nhapDienThoai){
        SQLiteDatabase database;
        db=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TenDT_Nhap",nhapDienThoai.getTenDT_Nhap());
        values.put("MaDt_Nhap", nhapDienThoai.getMaDT_Nhap());
        values.put("SoLuongNhap", nhapDienThoai.getSoLuong_Nhap());
        values.put("Gia", nhapDienThoai.getGia_Nhap());
        return db.insert("NhanVien", null, values);
    }
    //xoa dienthoai nhap

    public int deleteDienThoaiNhap(NhapDienThoai nhapDienThoai){
        return  db.delete("NhapDienThoai", "MaDT_Nhap=?", new
                String[]{String.valueOf(nhapDienThoai.getMaDT_Nhap())});
    }
}
